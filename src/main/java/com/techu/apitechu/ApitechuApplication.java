package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ApitechuApplication {

	public static List<ProductModel> productModel;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModel = ApitechuApplication.getTestData();
	}

	private static List<ProductModel> getTestData() {
		List<ProductModel> productModels = new ArrayList<>();

		productModels.add(new ProductModel("1", "Producto 1", 29.99f));
		productModels.add(new ProductModel("2", "Producto 2", 59.99f));
		productModels.add(new ProductModel("3", "Producto 3", 69.99f));
		productModels.add(new ProductModel("4", "Producto 4", 19.99f));

		return productModels;

	}

}
