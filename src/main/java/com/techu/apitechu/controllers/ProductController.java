package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    static final String APIBaseURL = "/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProduct");
        return ApitechuApplication.productModel;
    }

    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProduct");
        System.out.println("id: " + id);

        return ApitechuApplication.productModel.stream()
                .filter(productModel -> id.equals(productModel.getId()))
                .findAny().orElse(null);
    }

    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel product) {
        System.out.println("createProduct");
        System.out.println("product: " + product.toString());

        ApitechuApplication.productModel.add(product);

        return product;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel product) {
        System.out.println("updateProduct");

        ProductModel existingProduct = ApitechuApplication.productModel.stream()
                .filter(productModel -> id.equals(productModel.getId()))
                .findAny().orElse(null);

        if (Objects.isNull(existingProduct)) {
            return new ProductModel();
        }
        existingProduct.setDesc(product.getDesc());
        existingProduct.setPrice(product.getPrice());

        return existingProduct;
    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        ProductModel result = new ProductModel();

        ApitechuApplication.productModel.removeAll(
                ApitechuApplication.productModel.stream().filter(product -> product.getId().equals(id)).collect(Collectors.toList()));

        return result;
    }

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(@PathVariable String id, @RequestBody ProductModel product) {
        System.out.println("patchProduct");

        System.out.println("product desc: " + product.getDesc());
        System.out.println("product price: " + product.getPrice());

        ProductModel existingProduct = ApitechuApplication.productModel.stream()
                .filter(productModel -> id.equals(productModel.getId()))
                .findAny().orElse(null);

        if (Objects.nonNull(existingProduct)){
            if (Objects.nonNull(product.getDesc())) {
                existingProduct.setDesc(product.getDesc());
            }
            if (product.getPrice() > 0) {
                existingProduct.setPrice(product.getPrice());
            }

            return existingProduct;
        }
        return new ProductModel();
    }
}
